package u05lab.code

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Assertions._




class ExamManagerTest {

  import u05lab.code.ExamsManagerTest._


  /**
    * Si consultino le interfacce fornite (e i test corrispondenti sotto riportati):
    * - ExamResult che modella i possibili risultati di un esame universitario
    * - ExamResultFactory che modella una factory per denotare/creare i possibili risultati d'esame
    * - ExamsManager che modella la gestione dei risultati di esame di un certo corso, negli appelli di un anno
    *
    * Implementare ExamResultFactory e ExamsManager attraverso le classi ExamResultFactoryImpl e ExamsManagerImpl con
    * costruttori senza argomenti,
    * in modo che passino tutti i test di cui sotto.
    *
    * Sono considerati opzionali ai fini della possibilità di correggere l'esercizio, ma concorrono comunque al
    * raggiungimento
    * della totalità del punteggio:
    * - implementazione dei test opzionali (relativi al metodo ExamsManager.getBestResultFromStudent e ai
    * comportamenti con eccezione)
    * - la qualità della soluzione, in particolare con minimizzazione di ripetizioni e codice non inutilmente complesso
    *
    * Indicazioni di punteggio:
    * - correttezza della parte obbligatoria: 9 punti
    * - correttezza della parte opzionale: 3 punti
    * - qualità della soluzione: 5 punti
    *
    * Si tolga il commento al codice del test.
    */
  val erf: ExamResultFactory = new  ExamResultFactoryImpl()
  val em: ExamsManager =  new ExamsManagerImpl()

  // verifica base di ExamResultFactory
  @Test
  def testExamResultsBasicBehaviour() {


    // esame fallito, non c'è voto
    assertEquals(Kind.FAILED,erf.failed.getKind)
    assertTrue(erf.failed.getEvaluation.isEmpty)
    assertFalse(erf.failed.getEvaluation.isDefined)
    assertFalse(erf.failed.cumLaude)
    assertEquals("FAILED",erf.failed.toString)

    // lo studente si è ritirato, non c'è voto
    assertEquals(Kind.RETIRED,erf.retired.getKind)
    assertFalse(erf.retired.getEvaluation.isDefined)
    assertFalse(erf.retired.cumLaude)
    assertEquals("RETIRED",erf.retired.toString)

    // 30L
    assertEquals(Kind.SUCCEEDED,erf.succeededCumLaude.getKind)
    assertEquals(Some(30),erf.succeededCumLaude.getEvaluation)
    assertTrue(erf.succeededCumLaude.cumLaude)
    assertEquals(erf.succeededCumLaude.toString, "SUCCEEDED(30L)")

    // esame superato, ma non con lode
    assertEquals(Kind.SUCCEEDED, erf.succeeded(28).getKind)
    assertEquals(Some(28), erf.succeeded(28).getEvaluation)
    assertFalse(erf.succeeded(28).cumLaude)
    assertEquals("SUCCEEDED(28)", erf.succeeded(28).toString)
  }
  // verifica eccezione in ExamResultFactory
  @Test
  def testExceptionsExamResultFactory: Unit ={
    Assertions.assertThrows(classOf[IllegalArgumentException],() => erf.succeeded(32))

    // verifica eccezione in ExamResultFactory
    Assertions.assertThrows(classOf[IllegalArgumentException],() => erf.succeeded(17))

  }


  // metodo di creazione di una situazione di risultati in 3 appelli

  def prepareExams() {
    em.createNewCall("gennaio")
    em.createNewCall("febbraio")
    em.createNewCall("marzo")

    em.addStudentResult("gennaio", "rossi", erf.failed) // rossi -> fallito
    em.addStudentResult("gennaio", "bianchi", erf.retired) // bianchi -> ritirato
    em.addStudentResult("gennaio", "verdi", erf.succeeded(28)) // verdi -> 28
    em.addStudentResult("gennaio", "neri", erf.succeededCumLaude) // neri -> 30L
    em.addStudentResult("febbraio", "rossi", erf.failed) // etc..
    em.addStudentResult("febbraio", "bianchi", erf.succeeded(20))
    em.addStudentResult("febbraio", "verdi", erf.succeeded(30))

    em.addStudentResult("marzo", "rossi", erf.succeeded(25))
    em.addStudentResult("marzo", "bianchi", erf.succeeded(25))
    em.addStudentResult("marzo", "viola", erf.failed)
  }

  // verifica base della parte obbligatoria di ExamManager
  @Test
  def testExamsManagement(): Unit = {

    import scala.collection.immutable.HashSet
    this.prepareExams(); // partecipanti agli appelli di gennaio e marzo
    assertEquals(HashSet("rossi","bianchi", "verdi", "neri"), em.getAllStudentsFromCall("gennaio"))
    assertEquals(HashSet("rossi", "bianchi", "viola"), em.getAllStudentsFromCall("marzo"))

    // promossi di gennaio con voto
    assertEquals(2, em.getEvaluationsMapFromCall("gennaio").size)
    assertEquals(28, em.getEvaluationsMapFromCall("gennaio")("verdi").intValue())
    assertEquals(30, em.getEvaluationsMapFromCall("gennaio")("neri").intValue()) // promossi di febbraio con voto
    assertEquals(2, em.getEvaluationsMapFromCall("febbraio").size)
    assertEquals(20, em.getEvaluationsMapFromCall("febbraio")("bianchi").intValue())
    assertEquals(30, em.getEvaluationsMapFromCall("febbraio")("verdi").intValue())

    // tutti i risultati di rossi (attenzione ai toString!!)
    assertEquals(3, em.getResultsMapFromStudent("rossi").size)
    assertEquals("FAILED", em.getResultsMapFromStudent("rossi")("gennaio"))
    assertEquals("FAILED", em.getResultsMapFromStudent("rossi")("febbraio"))
    assertEquals("SUCCEEDED(25)", em.getResultsMapFromStudent("rossi")("marzo")) // tutti i risultati di bianchi
    assertEquals(3, em.getResultsMapFromStudent("bianchi").size)
    assertEquals("RETIRED", em.getResultsMapFromStudent("bianchi")("gennaio"))
    assertEquals("SUCCEEDED(20)", em.getResultsMapFromStudent("bianchi")("febbraio"))
    assertEquals("SUCCEEDED(25)", em.getResultsMapFromStudent("bianchi")("marzo")) // tutti i risultati di neri
    assertEquals(1, em.getResultsMapFromStudent("neri").size)
    assertEquals("SUCCEEDED(30L)", em.getResultsMapFromStudent("neri")("gennaio"))

  }

  // verifica del metodo ExamManager.getBestResultFromStudent
  @Test
  def optionalTestExamsManagement() {
    this.prepareExams(); // miglior voto acquisito da ogni studente, o vuoto..
    assertEquals(em.getBestResultFromStudent("rossi"), Some(25))
    assertEquals(em.getBestResultFromStudent("bianchi"), Some(25))
    assertEquals(em.getBestResultFromStudent("neri"), Some(30))
    assertEquals(em.getBestResultFromStudent("viola"), None)
  }

  @Test
  def testExceptionExamManager: Unit = {

    Assertions.assertThrows(classOf[IllegalArgumentException],() => {
      this.prepareExams()
      em.createNewCall("marzo")
    })
    Assertions.assertThrows(classOf[IllegalArgumentException],() => {
      this.prepareExams()
      em.addStudentResult("gennaio", "verdi", erf.failed)
    })
  }

}