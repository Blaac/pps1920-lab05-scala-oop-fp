package u05lab.code

import java.util.concurrent.TimeUnit

import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scala.concurrent.duration.FiniteDuration

object PerformanceUtils {

  case class MeasurementResults[T](result: T, duration: FiniteDuration) extends Ordered[MeasurementResults[_]] {
    override def compare(that: MeasurementResults[_]): Int = duration.toNanos.compareTo(that.duration.toNanos)
  }

  def measure[T](msg: String)(expr: => T): MeasurementResults[T] = {
    val startTime = System.nanoTime()
    val res = expr
    val duration = FiniteDuration(System.nanoTime() - startTime, TimeUnit.NANOSECONDS)
    if (!msg.isEmpty) println(msg + " -- " + duration.toNanos + " nanos; " + duration.toMillis + "ms")
    MeasurementResults(res, duration)
  }

  def measure[T](expr: => T): MeasurementResults[T] = measure("")(expr)
}


object CollectionsTest extends App {

  /* Linear sequences: List, ListBuffer */

  import scala.collection._

  val list = (1 to 1000000).toList
  val listb = list.to(collection.mutable.ListBuffer)

  /* Indexed sequences: Vector, Array, ArrayBuffer */
  val vect = list.to(immutable.Vector)
  val arr = list.to(scala.Array)
  var arrbuff = list.to(mutable.ArrayBuffer)

  /* Sets */
  val setimm = list.to(immutable.HashSet)
  var setmut = list.to(mutable.HashSet)


  /* Maps */
  val mapimm = immutable.HashMap((1 to 10000) -> (1 to 10000))
  var mapmut = mutable.HashMap((1 to 10000) -> (1 to 10000))

  /* Comparison */

  import PerformanceUtils._

  /* List vs listbuffer*/
  def measureIt[A](iterable: Iterable[A]) = {
    val name = iterable.getClass.toString
    measure(name + " last") {
      iterable.last
    }

    measure(name + " size") {
      iterable.size
    }

    measure(name + " tostring") {
      iterable.toString()
    }
    measure(name + " ++"){
      iterable ++ iterable
    }
  }


  def measureM[A, B](map: Map[A, B]) = {
    val name = map.getClass.toString
    measure(name + " keys") {
      map.keys
    }
    measure(name + " values") {
      map.values
    }
    measure(name + " keyset") {
      map
        .keySet
    }
  }

  measureIt(list)
  measureIt(listb)
  measureIt(vect)
  measureIt(arr)
  measureIt(arrbuff)
  measureIt(setimm)
  measureIt(setmut)
  measureIt(mapimm)
  measureIt(mapmut)
  measureM(mapimm)
  measureM(mapmut)


}