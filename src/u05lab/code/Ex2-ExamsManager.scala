package u05lab.code

import scala.collection.immutable.HashMap
import scala.collection.{Set, immutable, mutable}



object Kind {
  sealed trait EnumVal
  case object RETIRED extends EnumVal
  case object FAILED extends EnumVal
  case object SUCCEEDED extends EnumVal
  val kinds = Seq(RETIRED, FAILED, SUCCEEDED)
}


trait ExamResult {
  def getKind: Kind.EnumVal

  def getEvaluation: Option[Int]

  def cumLaude: Boolean
}

object ExamResult {
  def apply(kind: Kind.EnumVal, evaluation: Option[Int] , laude: Boolean): ExamResult = new ExamResult{

    override def getKind: Kind.EnumVal = kind

    override def getEvaluation: Option[Int] = evaluation

    override def cumLaude: Boolean = laude

    override def toString: String = {
      val sepL="("
      val sepR=")"
      val laudeS="L"

      if (laude)
        kind.toString+sepL+evaluation.get+laudeS+sepR
      else if (!evaluation.isEmpty)
        kind.toString+sepL+evaluation.get+sepR
      else
        kind.toString
    }

  }
}

trait ExamResultFactory {
  def failed: ExamResult

  def retired: ExamResult

  def succeededCumLaude: ExamResult

  def succeeded(evaluation: Int): ExamResult
}

class ExamResultFactoryImpl extends ExamResultFactory {
  override def failed: ExamResult = ExamResult(Kind.FAILED,None,false)

  override def retired: ExamResult = ExamResult(Kind.RETIRED,None,false)

  override def succeededCumLaude: ExamResult = ExamResult(Kind.SUCCEEDED,Some(30),true)

  override def succeeded(evaluation: Int): ExamResult = {
    if(evaluation < 18 || evaluation > 30)

      throw new IllegalArgumentException
    else
    ExamResult(Kind.SUCCEEDED,Some(evaluation),false)
  }
}





trait ExamsManager {
  def createNewCall(call: String): Unit

  def addStudentResult(call: String, student: String, result: ExamResult): Unit

  def getAllStudentsFromCall(call: String): Set[String]

  def getEvaluationsMapFromCall(call: String): Map[String,Int]

  def getResultsMapFromStudent(student: String): Map[String,String]

  def getBestResultFromStudent(student: String): Option[Int]
}

class ExamsManagerImpl extends ExamsManager {

  var map = new immutable.HashMap[String, immutable.HashMap[String,ExamResult]]

  private def checkArgument(condition: Boolean): Unit = {
    if (!condition) throw new IllegalArgumentException
  }

  override def createNewCall(call: String): Unit = {
    checkArgument(!map.contains(call))
    map += (call -> new immutable.HashMap[String,ExamResult])
  }

  override def addStudentResult(call: String, student: String, result: ExamResult): Unit = {
    checkArgument(map.contains(call))
    checkArgument(!map.get(call).contains(student))
    map += call -> (map(call) + (student -> result))
  }

  override def getAllStudentsFromCall(call: String): Set[String] = {
    checkArgument(map.contains(call))
    map(call).keySet
  }

  override def getEvaluationsMapFromCall(call: String): Map[String,Int] = {
    checkArgument(map.contains(call))
    map(call).filter(_._2.getEvaluation.isDefined).transform((_,v)=>v.getEvaluation.get)
  }

  override def getResultsMapFromStudent(student: String): Map[String,String] = {
    map.filter(_._2.contains(student)).transform((_,map)=>map(student).toString)
  }

  override def getBestResultFromStudent(student: String): Option[Int] = {
    map.filter(_._2.contains(student)).map({case (_,evals)=> evals(student).getEvaluation}).maxBy(_.isDefined)
  }
}





object ExamsManagerTest extends App {



}